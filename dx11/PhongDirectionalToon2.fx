
struct vsInput
{
    float4 posObject : POSITION;
	float4 normalObject : NORMAL;
};

struct psInput
{
    float4 posScreen : SV_Position;
	float3 pos : POSITION;
    float3 LightDirV: TEXCOORD0;
    float3 NormV: TEXCOORD1;
    float3 ViewDirV: TEXCOORD2;
};

struct vsInputTextured
{
    float4 posObject : POSITION;
	float4 normalObject : NORMAL;
	float4 uv: TEXCOORD0;
};

struct psInputTextured
{
    float4 posScreen : SV_Position;
	float3 pos : Position;
    float4 uv: TEXCOORD0;
    float3 LightDirV: TEXCOORD1;
    float3 NormV: TEXCOORD2;
    float3 ViewDirV: TEXCOORD3;
};

Texture2D inputTexture <string uiname="Texture";>;

SamplerState linearSampler <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
}; 

cbuffer cbPerDraw : register(b0)
{
	float4x4 tLAV: LAYERVIEW;
	float4x4 tV : VIEW;
	float4x4 tP: PROJECTION;
	float4x4 tLVP: LAYERVIEWPROJECTION;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float4x4 tWV: WORLDLAYERVIEW;
	float4x4 tWIT: WORLDINVERSETRANSPOSE;
	
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tColor <string uiname="Color Transform";>;
	

};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
};

#include "PhongDirectional.fxh"

psInputTextured VS_Textured(float4 PosO: POSITION,float3 NormO: NORMAL, float4 TexCd : TEXCOORD0)
{
    psInputTextured output;
  
    //inverse light direction in view space
    output.LightDirV = normalize(-mul(float4(lDir,0.0f), tV).xyz);
    
    //normal in view space
    output.NormV = normalize(mul(mul(NormO, (float3x3)tWIT),(float3x3)tLAV).xyz);

    //position (projected)
	output.pos = PosO.xyz;
    output.posScreen  = mul(PosO, mul(tW, tLVP));
    output.uv = mul(TexCd, tTex);
    output.ViewDirV = -normalize(mul(PosO, tWV).xyz);
    return output;
}

psInput VS(float4 PosO: POSITION,float3 NormO: NORMAL)
{
    psInput output;

    //inverse light direction in view space
    output.LightDirV = normalize(-mul(float4(lDir,0.0f), tV).xyz);
     
    //normal in view space
    output.NormV = normalize(mul(mul(NormO, (float3x3)tWIT),(float3x3)tLAV).xyz);

    //position (projected)
    output.posScreen  = mul(PosO, mul(tW, tLVP));
	output.pos = PosO.xyz;
    output.ViewDirV = -normalize(mul(PosO, tWV).xyz);
    return output;
}
    float4 ambientColorToon <bool color=true;String uiname="Ambient Color Toon";> = { 0.1,0.1,0.1,0.1};
	float4 lightColorToon <bool color=true;String uiname="Light Color Toon";> = { 1.0,1.0,1.0,1.0};
	float n_shades = 4;

float3 ComputeToonLighting(float3 normal, float3 lightDir)
{
    float3 color = ambientColorToon.rgb;
    float intensity = dot(normal, normalize(lightDir));
    //float topShading = smoothstep(intensity, 0.98, 0.0);
	float topShading = step(0.98, intensity);
    intensity = ceil(intensity * n_shades) / n_shades;
    intensity = max(intensity, ambientColorToon.r);
    color = lightColorToon.rgb * intensity;
	color += topShading * lerp(float3(1.0,1.0,1.0),lightColorToon.rgb,0.1);
    return color;
}


	float4 _SpecularColor;
			float _Glossiness;		

			float4 _RimColor;
			float _RimAmount;
			float _RimThreshold;

float4 ComputeToonLighting2(float3 normal, float3 lightDir, float3 viewDir)
{
 
	// Lighting below is calculated using Blinn-Phong,
	// with values thresholded to creat the "toon" look.
	// https://en.wikipedia.org/wiki/Blinn-Phong_shading_model

	// Calculate illumination from directional light.
	// _WorldSpaceLightPos0 is a vector pointing the OPPOSITE
	// direction of the main directional light.
	float NdotL = dot(lightDir, normal);

	// Samples the shadow map, returning a value in the 0...1 range,
	// where 0 is in the shadow, and 1 is not.
	//float shadow = SHADOW_ATTENUATION(i);
	// Partition the intensity into light and dark, smoothly interpolated
	// between the two to avoid a jagged break.
	float lightIntensity = smoothstep(0, 0.01, NdotL);	
	// Multiply by the main directional light's intensity and color.
	float4 light = lightIntensity * lightColorToon;

	// Calculate specular reflection.
	float3 halfVector = normalize(lightDir + viewDir);
	float NdotH = dot(normal, halfVector);
	// Multiply _Glossiness by itself to allow artist to use smaller
	// glossiness values in the inspector.
	float specularIntensity = pow(NdotH * lightIntensity, _Glossiness * _Glossiness);
	float specularIntensitySmooth = smoothstep(0.005, 0.01, specularIntensity);
	float4 specular = specularIntensitySmooth * _SpecularColor;				

	// Calculate rim lighting.
	float rimDot = 1 - dot(viewDir, normal);
	// We only want rim to appear on the lit side of the surface,
	// so multiply it by NdotL, raised to a power to smoothly blend it.
	float rimIntensity = rimDot * pow(NdotL, _RimThreshold);
	rimIntensity = smoothstep(_RimAmount - 0.01, _RimAmount + 0.01, rimIntensity);
	float4 rim = rimIntensity * _RimColor;



	return (light + ambientColorToon + specular + rim) * float4(1,1,1,1) ;
}

float4 PS_Textured(psInputTextured input): SV_Target
{
    float4 col = inputTexture.Sample(linearSampler, input.uv.xy);
	col.rgb = ComputeToonLighting(input.NormV, input.LightDirV);
	col.a = 1;
	return mul(col, tColor);
}

float4 PS(psInput input): SV_Target
{
    float4 col = 1;
	//col.rgb = ComputeToonLighting(input.NormV, input.LightDirV);
	col.rgb = ComputeToonLighting2(input.NormV, input.LightDirV, input.ViewDirV).rgb;
	col.a = 1;
    return mul(col, tColor);
}



technique10 GouraudDirectional <string noTexCdFallback="GouraudDirectionalNotexture"; >
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Textured() ) );
		SetPixelShader( CompileShader( ps_4_0, PS_Textured() ) );
	}
}

technique11 GouraudDirectionalNotexture
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}

