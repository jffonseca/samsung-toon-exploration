
struct vsInput
{
    float4 posObject : POSITION;
	float4 normalObject : NORMAL;
};

struct psInput
{
    float4 posScreen : SV_Position;
	float3 pos : POSITION;
    float3 LightDirV: TEXCOORD0;
    float3 NormV: TEXCOORD1;
    float3 ViewDirV: TEXCOORD2;
};

struct vsInputTextured
{
    float4 posObject : POSITION;
	float4 normalObject : NORMAL;
	float4 uv: TEXCOORD0;
};

struct psInputTextured
{
    float4 posScreen : SV_Position;
	float3 pos : Position;
    float4 uv: TEXCOORD0;
    float3 LightDirV: TEXCOORD1;
    float3 NormV: TEXCOORD2;
    float3 ViewDirV: TEXCOORD3;
};

Texture2D inputTexture <string uiname="Texture";>;

SamplerState linearSampler <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
}; 

cbuffer cbPerDraw : register(b0)
{
	float4x4 tLAV: LAYERVIEW;
	float4x4 tV : VIEW;
	float4x4 tP: PROJECTION;
	float4x4 tLVP: LAYERVIEWPROJECTION;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float4x4 tWV: WORLDLAYERVIEW;
	float4x4 tWIT: WORLDINVERSETRANSPOSE;
	
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tColor <string uiname="Color Transform";>;
	

};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
};

#include "PhongDirectional.fxh"

psInputTextured VS_Textured(float4 PosO: POSITION,float3 NormO: NORMAL, float4 TexCd : TEXCOORD0)
{
    psInputTextured output;
  
    //inverse light direction in view space
    output.LightDirV = normalize(-mul(float4(lDir,0.0f), tV).xyz);
    
    //normal in view space
    output.NormV = normalize(mul(mul(NormO, (float3x3)tWIT),(float3x3)tLAV).xyz);

    //position (projected)
	output.pos = PosO.xyz;
    output.posScreen  = mul(PosO, mul(tW, tLVP));
    output.uv = mul(TexCd, tTex);
    output.ViewDirV = -normalize(mul(PosO, tWV).xyz);
    return output;
}

psInput VS(float4 PosO: POSITION,float3 NormO: NORMAL)
{
    psInput output;

    //inverse light direction in view space
    output.LightDirV = normalize(-mul(float4(lDir,0.0f), tV).xyz);
     
    //normal in view space
    output.NormV = normalize(mul(mul(NormO, (float3x3)tWIT),(float3x3)tLAV).xyz);

    //position (projected)
    output.posScreen  = mul(PosO, mul(tW, tLVP));
	output.pos = PosO.xyz;
    output.ViewDirV = -normalize(mul(PosO, tWV).xyz);
    return output;
}
    float4 ambientColorToon <bool color=true;String uiname="Ambient Color Toon";> = { 0.1,0.1,0.1,0.1};
	float4 lightColorToon <bool color=true;String uiname="Light Color Toon";> = { 1.0,1.0,1.0,1.0};
	float n_shades = 4;

float3 ComputeToonLighting(float3 normal, float3 lightDir)
{
    float3 color = ambientColorToon.rgb;
    float intensity = dot(normal, normalize(lightDir));
    //float topShading = smoothstep(intensity, 0.98, 0.0);
	float topShading = step(0.98, intensity);
    intensity = ceil(intensity * n_shades) / n_shades;
    intensity = max(intensity, ambientColorToon.r);
    color = lightColorToon.rgb * intensity;
	color += topShading * lerp(float3(1.0,1.0,1.0),lightColorToon.rgb,0.1);
    return color;
}

float4 PS_Textured(psInputTextured input): SV_Target
{
    float4 col = inputTexture.Sample(linearSampler, input.uv.xy);
	col.rgb = ComputeToonLighting(input.NormV, input.LightDirV);
	col.a = 1;
	return mul(col, tColor);
}

float4 PS(psInput input): SV_Target
{
    float4 col = 1;
	col.rgb = ComputeToonLighting(input.NormV, input.LightDirV);
	col.a = 1;
    return mul(col, tColor);
}



technique10 GouraudDirectional <string noTexCdFallback="GouraudDirectionalNotexture"; >
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Textured() ) );
		SetPixelShader( CompileShader( ps_4_0, PS_Textured() ) );
	}
}

technique11 GouraudDirectionalNotexture
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}

